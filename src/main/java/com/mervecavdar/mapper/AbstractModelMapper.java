package com.mervecavdar.mapper;

public interface AbstractModelMapper<D, E> {

    D toDTO(E entity);

    E toEntity(D dto);

}
