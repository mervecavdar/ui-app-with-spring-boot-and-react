package com.mervecavdar.service;

import lombok.Getter;

@Getter
public abstract class RepoMapperHolder<R, M> {

    private final R repository;

    private final M mapper;

    protected RepoMapperHolder(R repository, M mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

}
