package com.mervecavdar.service;

import com.mervecavdar.dto.TaskDTO;
import com.mervecavdar.entity.Task;
import com.mervecavdar.mapper.TaskMapper;
import com.mervecavdar.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl extends RepoMapperHolder<TaskRepository, TaskMapper> implements TaskService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImpl.class);

    public TaskServiceImpl(TaskRepository taskRepository, TaskMapper taskMapper) {
        super(taskRepository, taskMapper);
    }

    @Override
    public List<TaskDTO> getAll() {
        List<Task> taskList = new ArrayList<>(getRepository().findAll());
        return taskList.stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public TaskDTO save(TaskDTO taskDTO) {
        Task task = toEntity(taskDTO);
        return toDTO(getRepository().save(task));
    }

    @Override
    public Optional<TaskDTO> findById(String id) {
        return getRepository().findById(id).map(this::toDTO);
    }

    @Override
    public void deleteById(String id) {
        LOGGER.debug("Deleting task by id: {}", id);
        getRepository().deleteById(id);
    }

    private TaskDTO toDTO(Task task) {
        return getMapper().toDTO(task);
    }

    private Task toEntity(TaskDTO taskDTO) {
        return getMapper().toEntity(taskDTO);
    }

}
