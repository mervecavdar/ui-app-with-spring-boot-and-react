package com.mervecavdar.service;

import java.util.List;
import java.util.Optional;

public interface AbstractCrudService<D, I> {

    List<D> getAll();

    D save(D dto);

    Optional<D> findById(I id);

    void deleteById(I id);

}
