package com.mervecavdar.service;

import com.mervecavdar.dto.UserDTO;
import com.mervecavdar.entity.User;
import com.mervecavdar.mapper.UserMapper;
import com.mervecavdar.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl extends RepoMapperHolder<UserRepository, UserMapper> implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        super(userRepository, userMapper);
    }

    @Override
    public List<UserDTO> getAll() {
        List<User> userList = new ArrayList<>(getRepository().findAll());
        return userList.stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        User user = toEntity(userDTO);
        return toDTO(getRepository().save(user));
    }

    @Override
    public Optional<UserDTO> findById(String identityNumber) {
        return getRepository().findById(identityNumber).map(this::toDTO);
    }

    @Override
    public void deleteById(String identityNumber) {
        LOGGER.debug("Deleting user by identityNumber: {}", identityNumber);
        getRepository().deleteById(identityNumber);
    }

    private UserDTO toDTO(User user) {
        return getMapper().toDTO(user);
    }

    private User toEntity(UserDTO userDTO) {
        return getMapper().toEntity(userDTO);
    }

}
