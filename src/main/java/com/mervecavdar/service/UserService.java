package com.mervecavdar.service;

import com.mervecavdar.dto.UserDTO;

public interface UserService extends AbstractCrudService<UserDTO, String> {

}
