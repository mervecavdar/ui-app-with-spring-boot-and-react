package com.mervecavdar.service;

import com.mervecavdar.dto.TaskDTO;

public interface TaskService extends AbstractCrudService<TaskDTO, String> {

}
