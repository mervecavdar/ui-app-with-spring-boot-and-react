package com.mervecavdar.dto;

import lombok.Data;

@Data
public class UserDTO {

    private String identityNumber;

    private String name;

    private String surname;

}
