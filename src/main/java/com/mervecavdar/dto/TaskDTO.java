package com.mervecavdar.dto;

import lombok.Data;

@Data
public class TaskDTO {

    private String id;

    private String priority;

    private String summary;

    private String description;

}
