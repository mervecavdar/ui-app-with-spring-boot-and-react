package com.mervecavdar.controller;

import com.mervecavdar.dto.UserDTO;
import com.mervecavdar.service.UserService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController implements CrudController<UserDTO, String> {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @Override
    @PutMapping
    public ResponseEntity<UserDTO> save(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.save(userDTO));
    }

    @Override
    @GetMapping("/{identityNumber}")
    public ResponseEntity<UserDTO> findById(@PathVariable String identityNumber) {
        return userService.findById(identityNumber)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Override
    @DeleteMapping("/{identityNumber}")
    public ResponseEntity<Void> deleteById(@PathVariable String identityNumber) {
        try {
            userService.deleteById(identityNumber);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}
