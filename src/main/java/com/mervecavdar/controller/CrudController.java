package com.mervecavdar.controller;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CrudController<D, I> {

    ResponseEntity<List<D>> getAll();

    ResponseEntity<D> save(D dto);

    ResponseEntity<D> findById(I id);

    ResponseEntity<Void> deleteById(I id);

}
