package com.mervecavdar.controller;

import com.mervecavdar.dto.TaskDTO;
import com.mervecavdar.service.TaskService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/task")
public class TaskController implements CrudController<TaskDTO, String> {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    @GetMapping
    public ResponseEntity<List<TaskDTO>> getAll() {
        return ResponseEntity.ok(taskService.getAll());
    }

    @Override
    @PutMapping
    public ResponseEntity<TaskDTO> save(@RequestBody TaskDTO taskDTO) {
        if (taskDTO.getId() == null) taskDTO.setId(UUID.randomUUID().toString());
        return ResponseEntity.ok(taskService.save(taskDTO));
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<TaskDTO> findById(@PathVariable String id) {
        return taskService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable String id) {
        try {
            taskService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}
