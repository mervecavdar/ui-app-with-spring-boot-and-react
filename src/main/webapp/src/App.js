import React from "react";
import { MainMenu } from "./components";
import { Container } from "semantic-ui-react";
import {
  EditTaskContainer,
  EditUserContainer,
  ListTaskContainer,
  ListUserContainer,
  NewTaskContainer,
  NewUserContainer,
} from "./containers";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

const App = () => {
  return (
    <Container fluid>
      <Router>
        <MainMenu />
        <div style={{ paddingLeft: "20%", paddingRight: "20%" }}>
          <Routes>
            <Route path="/" element={null} />
            <Route path="/users" element={<ListUserContainer />} />
            <Route path="/users/new" element={<NewUserContainer />} />
            <Route path="/users/:id/edit" element={<EditUserContainer />} />
            <Route path="/tasks" element={<ListTaskContainer />} />
            <Route path="/tasks/new" element={<NewTaskContainer />} />
            <Route path="/tasks/:id/edit" element={<EditTaskContainer />} />
          </Routes>
        </div>
      </Router>
    </Container>
  );
};

export default App;
