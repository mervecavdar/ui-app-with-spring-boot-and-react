import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Button, Icon } from "semantic-ui-react";
import { UserList } from "../views";

export const ListUserContainer = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const listUsers = () => {
    setLoading(true);
    axios
      .get("/api/user")
      .then(({ data }) => setData(data))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    listUsers();
  }, []);

  const handleDelete = (id) => {
    axios.delete(`/api/user/${id}`).then(() => listUsers());
  };

  return (
    <>
      <br />
      <Button primary as={Link} to="/users/new">
        <Icon name="user" />
        New User
      </Button>
      <UserList data={data} loading={loading} handleDelete={handleDelete} />
    </>
  );
};
