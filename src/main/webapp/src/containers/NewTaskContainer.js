import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { TaskForm } from "../views";
import { handleChange, taskHandleOnSubmit } from "../utils";
import { Divider, Header, Segment } from "semantic-ui-react";

export const NewTaskContainer = () => {
  const navigate = useNavigate();

  const [state, setState] = useState({
    id: null,
    priority: "",
    summary: "",
    description: "",
  });

  const taskHandleChange = (e, { name, value }) => {
    handleChange(state, setState, e, { name, value });
  };

  const handleOnSubmit = (e) => {
    taskHandleOnSubmit(state, navigate, e);
  };

  return (
    <>
      <br />
      <Segment padded="very" color={"blue"}>
        <Header as="h2" style={{ color: "#333333" }}>
          New Task
        </Header>
        <Divider />
        <TaskForm
          state={state}
          handleChange={taskHandleChange}
          handleOnSubmit={handleOnSubmit}
        />
      </Segment>
    </>
  );
};
