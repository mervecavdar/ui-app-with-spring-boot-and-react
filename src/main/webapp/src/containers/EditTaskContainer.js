import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { handleChange, taskHandleOnSubmit } from "../utils";
import { Divider, Header, Segment } from "semantic-ui-react";
import { TaskForm } from "../views";

export const EditTaskContainer = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [state, setState] = useState({
    id: null,
    priority: "",
    summary: "",
    description: "",
  });

  useEffect(() => {
    axios.get(`/api/task/${id}`).then((r) => setState(r.data));
  }, [id]);

  const taskHandleChange = (e, { name, value }) => {
    handleChange(state, setState, e, { name, value });
  };

  const handleOnSubmit = (e) => {
    taskHandleOnSubmit(state, navigate, e);
  };

  return (
    <>
      <br />
      <Segment padded="very" color={"blue"}>
        <Header as="h2" style={{ color: "#333333" }}>
          Edit Task
        </Header>
        <Divider />
        <TaskForm
          state={state}
          handleChange={taskHandleChange}
          handleOnSubmit={handleOnSubmit}
        />
      </Segment>
    </>
  );
};
