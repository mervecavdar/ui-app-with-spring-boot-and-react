import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Button, Icon } from "semantic-ui-react";
import { TaskList } from "../views";

export const ListTaskContainer = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const listTasks = () => {
    setLoading(true);
    axios
      .get("/api/task")
      .then(({ data }) => setData(data))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    listTasks();
  }, []);

  const handleDelete = (id) => {
    axios.delete(`/api/task/${id}`).then(() => listTasks());
  };

  return (
    <>
      <br />
      <Button primary as={Link} to="/tasks/new">
        <Icon name="tasks" />
        New Task
      </Button>
      <TaskList data={data} loading={loading} handleDelete={handleDelete} />
    </>
  );
};
