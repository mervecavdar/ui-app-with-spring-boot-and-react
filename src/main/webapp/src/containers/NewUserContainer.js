import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserForm } from "../views";
import { handleChange, userHandleOnSubmit } from "../utils";
import { Divider, Header, Segment } from "semantic-ui-react";

export const NewUserContainer = () => {
  const navigate = useNavigate();

  const [state, setState] = useState({
    identityNumber: "",
    name: "",
    surname: "",
  });

  const userHandleChange = (e, { name, value }) => {
    handleChange(state, setState, e, { name, value });
  };

  const handleOnSubmit = (e) => {
    userHandleOnSubmit(state, navigate, e);
  };

  return (
    <>
      <br />
      <Segment padded="very" color={"blue"}>
        <Header as="h2" style={{ color: "#333333" }}>
          New User
        </Header>
        <Divider />
        <UserForm
          state={state}
          handleChange={userHandleChange}
          handleOnSubmit={handleOnSubmit}
        />
      </Segment>
    </>
  );
};
