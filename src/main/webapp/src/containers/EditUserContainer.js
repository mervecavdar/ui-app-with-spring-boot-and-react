import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { UserForm } from "../views";
import { handleChange, userHandleOnSubmit } from "../utils";
import { Divider, Header, Segment } from "semantic-ui-react";

export const EditUserContainer = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [state, setState] = useState({
    identityNumber: "",
    name: "",
    surname: "",
  });

  useEffect(() => {
    axios.get(`/api/user/${id}`).then((r) => setState(r.data));
  }, [id]);

  const userHandleChange = (e, { name, value }) => {
    handleChange(state, setState, e, { name, value });
  };

  const handleOnSubmit = (e) => {
    userHandleOnSubmit(state, navigate, e);
  };

  return (
    <>
      <br />
      <Segment padded="very" color={"blue"}>
        <Header as="h2" style={{ color: "#333333" }}>
          Edit User
        </Header>
        <Divider />
        <UserForm
          id={id}
          state={state}
          handleChange={userHandleChange}
          handleOnSubmit={handleOnSubmit}
        />
      </Segment>
    </>
  );
};
