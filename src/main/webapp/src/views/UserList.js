import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Dropdown,
  Header,
  Icon,
  Modal,
  Table,
} from "semantic-ui-react";
import { Loader } from "../components";

export const UserList = ({ loading, data, handleDelete }) => {
  const [openModal, setOpenModal] = useState(false);
  const [id, setId] = useState("");

  const showDeleteModal = (identityNumber) => {
    setId(identityNumber);
    setOpenModal(true);
  };

  const deleteUser = () => {
    setOpenModal(false);
    handleDelete(id);
  };

  return (
    <>
      <Table celled color={"blue"} key={"blue"}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={1} />
            <Table.HeaderCell width={5}>Identity Number</Table.HeaderCell>
            <Table.HeaderCell width={5}>Name</Table.HeaderCell>
            <Table.HeaderCell width={5}>Surname</Table.HeaderCell>
            <Table.HeaderCell width={1} />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {data.map((item) => {
            const { identityNumber, name, surname } = item;
            return (
              <Table.Row key={identityNumber}>
                <Table.Cell textAlign="center">
                  <Icon name="user" />
                </Table.Cell>
                <Table.Cell>{identityNumber}</Table.Cell>
                <Table.Cell>{name}</Table.Cell>
                <Table.Cell>{surname}</Table.Cell>
                <Table.Cell textAlign="center">
                  <Dropdown icon="cog">
                    <Dropdown.Menu>
                      <Dropdown.Item
                        icon={"edit"}
                        text="Edit"
                        as={Link}
                        to={`/users/${identityNumber}/edit`}
                      />
                      <Dropdown.Item
                        icon={"delete"}
                        text="Delete"
                        onClick={() => showDeleteModal(identityNumber)}
                      />
                    </Dropdown.Menu>
                  </Dropdown>
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>

      <Modal
        closeIcon
        open={openModal}
        onClose={() => setOpenModal(false)}
        onOpen={() => setOpenModal(true)}
      >
        <Header icon="delete" content="Delete User" />
        <Modal.Content>
          <p>Do you want to delete the selected user?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={() => setOpenModal(false)}>
            <Icon name="delete" /> No
          </Button>
          <Button color="green" onClick={() => deleteUser()}>
            <Icon name="checkmark" /> Yes
          </Button>
        </Modal.Actions>
      </Modal>

      {loading ? <Loader /> : null}
    </>
  );
};
