import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Dropdown,
  Header,
  Icon,
  Modal,
  Table,
} from "semantic-ui-react";
import { Loader } from "../components";

export const TaskList = ({ loading, data, handleDelete }) => {
  const [openModal, setOpenModal] = useState(false);
  const [id, setId] = useState("");

  const showDeleteModal = (id) => {
    setId(id);
    setOpenModal(true);
  };

  const deleteTask = () => {
    setOpenModal(false);
    handleDelete(id);
  };

  return (
    <>
      <Table celled color={"blue"} key={"blue"}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={1} />
            <Table.HeaderCell width={1}>Priority</Table.HeaderCell>
            <Table.HeaderCell width={6}>Summary</Table.HeaderCell>
            <Table.HeaderCell width={7}>Description</Table.HeaderCell>
            <Table.HeaderCell width={1} />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {data.map((item) => {
            const { id, priority, summary, description } = item;
            return (
              <Table.Row key={id}>
                <Table.Cell textAlign="center">
                  <Icon name="tasks" />
                </Table.Cell>
                <Table.Cell textAlign="center">
                  {priority === "LOW" ? (
                    <Icon color={"green"} name={"exclamation"} />
                  ) : (
                    <Icon color={"red"} name={"exclamation"} />
                  )}
                </Table.Cell>
                <Table.Cell>{summary}</Table.Cell>
                <Table.Cell>{description}</Table.Cell>
                <Table.Cell textAlign="center">
                  <Dropdown icon="cog">
                    <Dropdown.Menu>
                      <Dropdown.Item
                        icon={"edit"}
                        text="Edit"
                        as={Link}
                        to={`/tasks/${id}/edit`}
                      />
                      <Dropdown.Item
                        icon={"delete"}
                        text="Delete"
                        onClick={() => showDeleteModal(id)}
                      />
                    </Dropdown.Menu>
                  </Dropdown>
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>

      <Modal
        closeIcon
        open={openModal}
        onClose={() => setOpenModal(false)}
        onOpen={() => setOpenModal(true)}
      >
        <Header icon="delete" content="Delete Task" />
        <Modal.Content>
          <p>Do you want to delete the selected task?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={() => setOpenModal(false)}>
            <Icon name="delete" /> No
          </Button>
          <Button color="green" onClick={() => deleteTask()}>
            <Icon name="checkmark" /> Yes
          </Button>
        </Modal.Actions>
      </Modal>

      {loading ? <Loader /> : null}
    </>
  );
};
