import React from "react";
import { Form, Icon } from "semantic-ui-react";

export const UserForm = ({ id, state, handleChange, handleOnSubmit }) => {
  return (
    <Form onSubmit={handleOnSubmit}>
      <Form.Input
        type={"number"}
        name="identityNumber"
        label="Identity Number"
        onChange={handleChange}
        value={state.identityNumber}
        disabled={!!id}
        required
      />
      <Form.Input
        name="name"
        label="Name"
        onChange={handleChange}
        value={state.name}
        required
      />
      <Form.Input
        name="surname"
        label="Surname"
        onChange={handleChange}
        value={state.surname}
        required
      />
      <Form.Button primary>
        <Icon name="save" />
        Save
      </Form.Button>
    </Form>
  );
};
