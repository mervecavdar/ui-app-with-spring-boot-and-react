import React from "react";
import { Form, Icon } from "semantic-ui-react";

export const TaskForm = ({ state, handleChange, handleOnSubmit }) => {
  const priorityOptions = [
    { key: "LOW", text: "Low", value: "LOW" },
    { key: "HIGH", text: "High", value: "HIGH" },
  ];

  return (
    <Form onSubmit={handleOnSubmit}>
      <Form.Input name="id" value={state.id} style={{ display: "none" }} />
      <Form.Dropdown
        name="priority"
        label="Priority"
        fluid
        selection
        required
        options={priorityOptions}
        onChange={handleChange}
        value={state.priority}
      />
      <Form.Input
        name="summary"
        label="Summary"
        onChange={handleChange}
        value={state.summary}
        required
      />
      <Form.TextArea
        name="description"
        label="Description"
        onChange={handleChange}
        value={state.description}
        required
      />
      <Form.Button primary>
        <Icon name="save" />
        Save
      </Form.Button>
    </Form>
  );
};
