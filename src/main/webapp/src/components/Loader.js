import React from "react";
import { Dimmer, Loader as SemanticLoader, Segment } from "semantic-ui-react";

export const Loader = () => {
  return (
    <Segment basic>
      <Dimmer active inverted border="none">
        <SemanticLoader>Loading...</SemanticLoader>
      </Dimmer>
    </Segment>
  );
};
