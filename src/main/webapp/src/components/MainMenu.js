import React, { useState } from "react";
import { Icon, Menu } from "semantic-ui-react";
import { Link, useLocation } from "react-router-dom";

export const MainMenu = () => {
  const location = useLocation();

  const getCurrent = () => {
    const mainPath = location.pathname.split("/")[1];
    return !mainPath ? "home" : mainPath;
  };

  const [state, setState] = useState({ activeItem: getCurrent() });

  const handleItemClick = (e, { name }) => {
    setState({ activeItem: name });
  };

  return (
    <div>
      <Menu pointing secondary>
        <Menu.Item
          name="home"
          active={state.activeItem === "home"}
          onClick={handleItemClick}
          as={Link}
          to="/"
        />
        <Menu.Item
          name="users"
          active={state.activeItem === "users"}
          onClick={handleItemClick}
          as={Link}
          to="/users"
        >
          Users
        </Menu.Item>
        <Menu.Item
            name="tasks"
            active={state.activeItem === "tasks"}
            onClick={handleItemClick}
            as={Link}
            to="/tasks"
        >
          Tasks
        </Menu.Item>
        <Menu.Item position="right">
          <Icon size={"large"} name={"sign-out alternate"} />
        </Menu.Item>
      </Menu>
    </div>
  );
};
