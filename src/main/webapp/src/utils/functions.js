import axios from "axios";

export const handleChange = (state, setState, e, { name, value }) => {
  setState({ ...state, [name]: value });
};

export const userHandleOnSubmit = (state, navigate) => {
  axios.put("/api/user", state).then(() => navigate("/users"));
};

export const taskHandleOnSubmit = (state, navigate) => {
  axios.put("/api/task", state).then(() => navigate("/tasks"));
};
