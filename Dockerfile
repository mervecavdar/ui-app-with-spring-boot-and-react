FROM adoptopenjdk:11-jre-hotspot
EXPOSE 5001
ADD target/ui-app-with-spring-boot-and-react.jar ui-app-with-spring-boot-and-react.jar
ENTRYPOINT ["java", "-jar", "/ui-app-with-spring-boot-and-react.jar"]
