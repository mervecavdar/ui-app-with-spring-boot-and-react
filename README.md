# UI Application With Spring Boot And React

## For backend: 
1. mvn clean install
2. Run backend

## For frontend:
1. cd src/main/webapp
2. npm install
3. npm start